<!-- [GitLab版 GFM](https://docs.gitlab.com/ee/user/markdown.html)   -->
<!-- [GitHub版 GFM](https://github.github.com/gfm/) -->

# Pseudo Italic Font : なんちゃって斜体フォント

asciidoctor-pdf を使いたいのだけれど、フリーの斜体フォントを探すのが面倒に感じたので作ってみました。

見栄えや完成度は気にせず「筆者がイタリック体で表現していることをきちんと伝える」ことをゴールにしました。



## 2021/02/23 時点でわかったこと
 - 自分の環境 debian/bullseye/amd64 (testing) の fontforge では segmentation fault 起こす
 - python3-fontforge でスクリプト書いてみたら通った不思議

## スクリプト make-pseudo-italic.py の使い方

`python3 make-pseudo-italic.py <input-ttf-file>` を実行するとカレントディレクトリに `<basename-of-input-ttf-file>-PseudoItalic.ttf` が作成されます。

中身は「フォントの中身全部選んで -19度で italicize() する」だけのシンプルなもの


## ここで配布しているフォントファイル

ライセンスは [SIL Open Font License 1.1](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web) です。
不適切な点があれば knaga.rgj at gmail.com までお願いします。

| font file                                | source font file            | source URL                               | source license | source download URL                                                                   |
|------------------------------------------|-----------------------------|------------------------------------------|----------------|---------------------------------------------------------------------------------------|
| GenYoMinJP-Regular-PseudoItalic.ttf      | GenYoMinJP-Regular.ttf      | https://github.com/ButTaiwan/genyo-font/ | SIL OFL 1.1    | https://github.com/ButTaiwan/genyo-font/tree/bc2fa246196fefc1ef9e9843bc8cdba22523a39d |
| GenYoMinJP-Bold-PseudoItalic.ttf         | GenYoMinJP-Bold.ttf         | same as above                            | same as above  | same as above                                                                         |
| GenShinGothic-P-Regular-PseudoItalic.ttf | GenShinGothic-P-Regular.ttf | http://jikasei.me/font/genshin/          | SIL OFL 1.1    | https://onedrive.live.com/redir?resid=9882014220136771%21108                          |
| GenShinGothic-P-Bold-PseudoItalic.ttf    | GenShinGothic-P-Bold.ttf    | same as above                            | same as above  | same as above                                                                         |

```
![源樣明體/源様明朝](https://buttaiwan.github.io/font/pics/genyo.png)

# 繁體中文

「源樣明體」是基於[思源宋體](https://github.com/adobe-fonts/source-han-serif/)的開放原始碼中文字型。
採用思源宋體韓文（KR）版本的字符，配合繁體中文慣用的置中標點，可排版傳統印刷體風格的文件。

## 特徵

### 傳統印刷體

思源宋體預設的台灣版本（TW）採用教育部標準字體，雖然適合教育用途，但不盡符合傳統印刷體審美觀。
例如點、挑、撇筆畫太多，文件排列時的的跳動感較大。
此專案採用KR版本的字符為主，較接近傳統印刷體習慣，排列感較為整齊。

> Note: 本專案並沒有造新的字符，所有字符都是來自思源宋體本身。所以KR版本未收錄的罕用字或簡化字，仍然會是TW、CN風格。

### 漢字不缺字

保留思源宋體所支援之所有漢字（包或日、韓文漢字與簡體字），一般會用到的漢字幾乎不會有缺字問題。
同時也保留了所有日文假名。（但刪除了高達1萬字的韓文字，有效降低字型檔大小。）

> Note: 本專案並沒有造新的字符，簡體字等當然會是CN風格。

### 標點符合繁體中文習慣

直接使用思源宋體KR版本排中文，會有標點置於左下的問題。
此版本的標點符號仍採用繁體中文習慣，逗號、句號等都置於正中央。
並且系統能正確識別為繁體中文字型。

另外，思源宋體的標點符號如「，」「。」設定有比例寬度的壓縮字寬，在Illustrator、InDesign等環境下預設會壓縮50%顯示。這往往會讓標點顯得過窄。
此版本移除了部分符號的壓縮字寬資訊，讓這些標點符號字寬原則上保持全形。

### TrueType格式

字型已經轉換成TrueType格式（.ttf）。
在Word、PowerPoint等軟體支援都較為完整（例如可以正常內嵌在投影片裡）。
對一些老舊的應用程式相容性都較高。

### Ver 1.200　更新 (2018/8/19)

* 解決Word等環境直排時標點符號無法正常旋轉的問題。 （Word 2007 / Word 2016 測試成功。轉成PDF亦正常。）

### Ver 1.300　更新 (2018/9/24)

* 修改部分KR版本不理想的文字對應，並統一部份文字的部件。

### 解決一些其他思源宋體使用上的雜問題

* ‘’“” 四個符號，改用半形版本，讓 It’s 之類英文排列正常。（因台灣多用「」引號，少用“”）
* 解決在Illustrator中，文字行高過高，不容易選到正確行的問題。
* 解決在Word中，文字佔兩倍行高的問題。（12pt仍然會占2倍行高，但11pt就能在1倍行高內顯示了。）

## 下載字型

請點選GitHub此畫面右上綠色「Clone or download」按鈕，並選擇「Download ZIP」。

## 此為公測版

目前仍是Beta公測版本，未來本專案不排除有大幅更改規格的可能性。
歡迎提供測試反饋，請直接反饋在GitHub的Issues中。
未來亦不保證持續跟隨思源宋體升版。

## 著作權與授權

* 本字型是基於 SIL Open Font License 1.1 改造Adobe所開發、發表的「[思源宋體](https://github.com/adobe-fonts/source-han-serif/)」字型。

* 本字型亦基於 SIL Open Font License 1.1 授權條款免費公開，關於授權合約的內容、免責事項等細節，請詳讀 License 文件。

    * 本字型可自由使用在印刷、影像、網路或任何媒體上，不限個人或商業使用。
    * 您可基於 SIL Open Font License 1.1 的規定再散佈或改造本字型。

---

# 日本語
「源様明朝」は[源ノ明朝](https://github.com/adobe-fonts/source-han-serif/)から派生されたフリーフォントです。

## 特徴

### 源ノ明朝のそのままTrueType化

* 日本語ver.は源ノ明朝をほぼそのままTrueType化しただけ。OpenTypeに対応していないアプリケーションでの利用を想定しています。
* Illustratorで、文字選択用のバウンディングボックスがすごく高くなっている問題を解消しました。（すごく高い→やや高いｗ）
* Wordで、行間がすごく高い問題をある程度解消しました。

### 文字セット

日本語サブセットではなく、繁体字中国語セット（Big5）の漢字も入っています。
漢字なら比較的文字化けはしないと思います。
簡体字・ハングルなどはありません。

### Ver 1.200　更新 (2018/8/19)

* WordなどGDI環境で、句読点などが正しく回転できない問題を解決しました。（Word 2007 / Word 2016 テスト済み。PDF保存もOK。）

### Ver 1.300　更新 (2018/9/24)

* 一部の漢字を筆押さえのある字形に変更。
![img](https://user-images.githubusercontent.com/5418570/45954269-2457b300-c03f-11e8-889d-af3db2860a8b.png)

## ダウンロード

このGitHubページの右上の「Clone or download」ボタンから、「Download ZIP」をクリックしてください。

## ベータバージョンです

これはベータバージョンです。
予告なく大幅に仕様が変わる可能性あり。

## ライセンスと著作権

* このフォントは、Adobe社が開発・公開している「[源ノ明朝](https://github.com/adobe-fonts/source-han-serif/)」を SIL Open Font License 1.1 のもとで改変したフォントです。

* このフォントは SIL Open Font License 1.1 のもとで無償にて公開します。免責条項などに関してはライセンスをご確認ください。
　
    * 利用者はこのフォントを使用した制作物を有償・無償を問わず自由に公開・配布することが出来ます。 　
    * このフォントに対して SIL Open Font License 1.1 で定められた制限のもとで再配布や改変を行うことが出来ます。

```


```
源真ゴシック (げんしんゴシック)
Version 1.002.20150607


■ このフォントについて

源真ゴシック (げんしんゴシック) は、フリーの OpenType フォントである
「源ノ角ゴシック (Noto Sans CJK / Source Han Sans の日本語部分)」を
TrueType 形式に変換し、使い勝手を向上するカスタマイズを施したフォントです。

詳細は、以下のサイトをごらんください。
http://jikasei.me/font/genshin/


■ ライセンスと著作権について

・源真ゴシックは源ノ角ゴシックを改変して制作したもので、M+ OUTLINE FONTS 由来の文字グリフも一部含みます。
・フォントデータに含まれる、源ノ角ゴシック由来の文字グリフの著作権は Adobe が所有しています。
・フォントデータに含まれる、M+ OUTLINE FONTS 由来の文字グリフの著作権は M+ FONTS PROJECT が所有しています。
・源真ゴシックのフォントファイルは、源ノ角ゴシックと同じ SIL Open Font License 1.1 のもとで使用することができます。

SIL Open Font License 1.1 の内容は、アーカイブに同梱の LICENSE.txt に記載されています。
この日本語訳は、以下から参照することができます。
http://osdn.jp/projects/opensource/wiki/SIL_Open_Font_License_1.1

M+ OUTLINE FONTS のグリフは、同梱のファイル LICENSE_J に記載された自由な
M+ FONTS LICENSE に基づき使用しています。


■ 頒布元

源真ゴシックの最新版は、以下のサイトで頒布しています。
不具合などが修正された場合、新しいバージョンとして公開されますので、
定期的にサイトをご確認いただければ幸いです。

自家製フォント工房
http://jikasei.me/
```



感謝: https://tech-blog.cloud-config.jp/2020-05-11-github-actions-asciidoc-pdf2/

