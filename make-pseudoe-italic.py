#!/usr/bin/python3

# fontforge binding は戻り値も Excption もなくエラーが出力される残念仕様

import os
import fontforge

def conv(fname, is_individual=False):
    (base,ext) = os.path.splitext(os.path.basename(fname))
    ofname = base+"-PseudoItalic"+ext
    print("converting from %s to %s..."%(fname, ofname))
    fnt = fontforge.open(fname)
    # unicode range: 0x000020 - 0x10FFFF
    if is_individual:
        for uc in range(0x000020, 0x110000):
            print("0x%06X"%(uc))
            fnt.selection.select(uc)
            fnt.italicize(-19)
            pass
    else:
        fnt.selection.all()
        fnt.italicize(-19)
        pass
    fnt.generate(ofname)
    return


if __name__ == '__main__':
    import sys
    for fname in sys.argv[1:]:
        conv(fname)
        pass
    pass


#
# excpet Exception as e したら Ctrl-C で中断できない
#
