#!/bin/sh -x

rm -rf *ttf

wget -O genyo-font-bc2fa246196fefc1ef9e9843bc8cdba22523a39d.zip https://github.com/ButTaiwan/genyo-font/archive/bc2fa246196fefc1ef9e9843bc8cdba22523a39d.zip
unzip -j genyo-font-bc2fa246196fefc1ef9e9843bc8cdba22523a39d.zip genyo-font-bc2fa246196fefc1ef9e9843bc8cdba22523a39d/JP/GenYoMinJP-Regular.ttf genyo-font-bc2fa246196fefc1ef9e9843bc8cdba22523a39d/JP/GenYoMinJP-Bold.ttf

wget -O genshingothic-20150607.7z "https://onedrive.live.com/download?resid=9882014220136771%21342"
7zr x genshingothic-20150607.7z GenShinGothic-P-Regular.ttf GenShinGothic-P-Bold.ttf

python3 ./make-pseudoe-italic.py GenYoMinJP-Regular.ttf GenYoMinJP-Bold.ttf GenShinGothic-P-Regular.ttf GenShinGothic-P-Bold.ttf
